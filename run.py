import os
import FileCompressor
import FileEncrypter
import time

global found
global running
rootDir = "/home/pi/IMU/" 
arcDir = "/home/pi/IMU/Archive/"
outDir = "/home/pi/IMU/OutputFiles/"
cObj = FileCompressor.Compressor()
eObj = FileEncrypter.Encrypter()
running = True

def runState():
    global found
    global running
    found = True
    scanForNewFiles()
    if found == False:
        print("No new data found")
    decom = input("Would you like to decrypt and decompress a file? Y/N ")  
    if decom == "Y":
        de_compcrypt()
    else: 
        keepRunning = input("Would you like to keep running? Y/N ")
        if keepRunning == "N":
             running = False
        
             
def de_compcrypt():
    file = input("enter file for decryption and decompression ")
    start = time.time()
    eObj.decrypt(arcDir+file)
    cObj.decompress("dec.zip",outDir,file[0:4]+"Prc.csv")
    end = time.time()
    decTime = round(end-start,2)
    print("------------------------- de_compcrypt time",decTime)
    os.remove(os.path.join(rootDir, "dec.zip"))
    print(file+ " decrypted and decompressed")
    

def scanForNewFiles():
    global found
    for file in os.listdir(rootDir):
        if (file[-3:] == "csv"):
            start = time.time()
            cObj.compress(file)
            print(file + " compressed")
            zfile = arcDir+file[0:-3]+ "zip"
            eObj.encrypt(zfile)
            end = time.time()
            decTime = round(end-start,2)
            print("------------------------- compenc time",decTime)
            print(file + " encrypted")
            os.remove(os.path.join(rootDir, file))
        else:
            found = False
            
if __name__ == "__main__":
    try:
        while running == True:
           runState()
           pass
    except Exception as e:
        print(e)


