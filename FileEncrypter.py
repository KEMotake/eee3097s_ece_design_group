from cryptography.fernet import Fernet
import sys
import os
import time

class Encrypter:
    def encrypt(self,filename):
        key = Fernet.generate_key()
        with open('mykey.txt', 'wb') as mykey:
            mykey.write(key)
        with open('mykey.txt', 'rb') as mykey:
            key = mykey.read()
        keySize=sys.getsizeof(key)
        print("\nThe key size is", keySize,'bytes')
        print(key)
        fernet = Fernet(key)
        with open(filename, 'rb') as comFile:
            compressedFile = comFile.read()
        oriSize=os.path.getsize(filename)
        print("\nThe size of the compressed data is (Before)", oriSize,'bytes')
        start = time.time()
        encryptedData=fernet.encrypt(compressedFile)
        end = time.time()
        encTime = round(end-start,2)
        print("The execution time of encrypting the compressed data is ", encTime, ' seconds')
        with open (filename, 'wb') as encrypted_file:
            encrypted_file.write(encryptedData)   
        encSize=os.path.getsize(filename)
        print("The Size of the encrypted data is (After)", encSize, 'bytes')
        print("Data size difference between compressed and encrypted file is", encSize-oriSize,'bytes')
        print("Encryption speed (Uncompressed bits/seconds to compress)",round(oriSize/round(encTime,2)*0.000001,2),'Mbits/s')

    def decrypt(self,filename):
        with open('mykey.txt', 'rb') as mykey:
            key = mykey.read()
        with open(filename, 'rb') as encFile:
            encryptedData = encFile.read()
        encSize=os.path.getsize(filename)
        print("\nThe Size of the encrypted data is (Before)", encSize, 'bytes')
        fernet = Fernet(key)
        start = time.time()
        decryptedData = fernet.decrypt(encryptedData)
        end = time.time()
        decTime = round(end-start,2)
        print("The execution time of decrypting the compressed data is ", decTime, ' seconds')
        with open("dec.zip", 'wb') as decrypted_file:
            decrypted_file.write(decryptedData)
        decSize=os.path.getsize("dec.zip")
        print("The Size of the decrypted data is (After)", decSize, 'bytes')
        print("Data size difference between encrypted and decrypted file is", encSize-decSize,'bytes')
        print("Decryption speed (encrypted bits/seconds to decrypt)",round(encSize/round(decTime,2)*0.000001,2),'Mbits/s')
        

