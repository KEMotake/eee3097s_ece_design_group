import os
import datetime 
import math
import time
import smbus
import ICM20948
import LPS22HB


icm20948=ICM20948.ICM20948()
lps22hb = LPS22HB.LPS22HB()
if __name__ == '__main__':
  while True:
    filename ="IMU_data.csv" #.format(datetime.datetime.now())
    f = open(filename, "w")
    f.write("UTC time,magx,magy,magz,accx,accy,accz,gyrox,gyroy,gyroz,press,temp,pitch,rol,yaw\n")
    startTime = time.time() 
    endTime = 0
    while (endTime <= 60):
    	x = datetime.datetime.now()
    	magx,magy,magz,accx,accy,accz,gyrox,gyroy,gyroz,pitch,rol,yaw = icm20948.run()
    	press,temp = lps22hb.run()
    	startfile = time.time()
    	data_entry = "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(x,magx,magy,magz,accx,accy,accz,gyrox,gyroy,gyroz,press,temp,pitch,rol,yaw)
    	f.write(data_entry)
    	time.sleep(0.1)
    	endTime = time.time() - startTime
    	print(endTime)
    f.close()
    print("file closed")
