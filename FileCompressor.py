import time
import sys
import os
import zipfile
import zlib

class Compressor:
        
    def compress(self,file):
        dataFile = file 
        CompressedFile ="Archive/" +file[0:-4]+ ".zip"
        with open(dataFile, mode="rb") as fin, open(CompressedFile, mode="wb") as fout:
            data = fin.read()
            compressed_data = zlib.compress(data, zlib.Z_BEST_COMPRESSION)
            print(f"Original size: {sys.getsizeof(data)}")
            # Original size: 1000033
            print(f"Compressed size: {sys.getsizeof(compressed_data)}")
            # Compressed size: 1024
            fout.write(compressed_data)
        
        
    def decompress(self,file,dir,finalfile):
        with open(file, mode="rb") as fin, open(dir+finalfile, mode="wb") as fout:
            data = fin.read()
            compressed_data = zlib.decompress(data)
            print(f"Compressed size: {sys.getsizeof(data)}")
            # Compressed size: 1024
            print(f"Decompressed size: {sys.getsizeof(compressed_data)}")
            # Decompressed size: 1000033
            fout.write(compressed_data)



        #with zipfile.ZipFile(file, 'r') as zip_ref:
        #   decSize = os.path.getsize(file)
        #   print("\nThe Size of the decrypted data is (Before)", decSize, 'bytes')
        #   start = time.time()
        #   zip_ref.extractall(dir)
        #   end = time.time()
        #   decTime = round(end-start,2)
        #   print("The execution time of decompressing the decrypted data is ", decTime, ' seconds')
        #   decompSize=os.path.getsize(dir+finalfile[0:-4]+".csv")
        #   print("The Size of the decompressed data is (After)", decompSize, 'bytes')
        #   print("Data size difference between the original and compressed file is", decSize-decompSize,'bytes')
        #   print("decompression speed (decrypted bits/seconds to compress)",round(decSize/round(decTime,2)*0.000001,2),'Mbits/s')


